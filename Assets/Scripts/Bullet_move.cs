﻿using UnityEngine;
using System.Collections;

public class Bullet_move : MonoBehaviour {
    
    public float speed;             //The speed of the bullet
    private Rigidbody2D rb;         //The body of the bullet

    private int value;              //The value the bullet has
    private Vector3 targetPos;
    private Vector3 startPos;
    private float length;
    private string currTar;         //Name of the taget the bullets is shot at

    //For the shot parabole
    private float a = -0.1f;        //Variable for changing the maximum of the parabole
    private float b;                //Ensures the target is at y = 0
    private float x;                //Variable for the length to the target
    private float y;                //Variable for the scale
    private float offset = 0.9f;    //Variable to offset the target hit, ensures the bullet will have the right scale

    Vector3 startScale;             //For saving the start scale

    GameController gameController;  //The gamecontroller. Currently Canon


    void Start()
    {
        //Get the rigidbody2d component
        rb = GetComponent<Rigidbody2D>();
        //Set the speed and direction of the bullet
        rb.velocity = transform.right * speed;

        //Save the reference to GameController
        gameController = GameObject.Find("Canon").GetComponent<GameController>();
        //Save the targetposition
        targetPos = gameController.getTargetPos();

        //Save the start position of the bullet
        startPos = transform.position;
        length = getLength(startPos, targetPos);

        //Save the starting scale
        startScale = transform.localScale;

        //Calculate b
        b = -a * (length - offset);

        //Save the string of what we are shooting at
        currTar = gameController.getCurrentTar();
    }

    void Update()
    {
        x = getLength(startPos, transform.position);
        y = a * Mathf.Pow(x,2) + b * x + 1;
        //Debug.Log("x: " + x + "  ;  y: " + y);

        //Changes the scale of the bullet to make it look like a parabole
        transform.localScale = startScale * y;

        //If it somehow misses, this ensures the bullet doesn't glitch out
        if (transform.localScale.y < 0.5)
        {
            Destroy(gameObject);
        }
    }

    float getLength(Vector3 start, Vector3 end)
    {
        return Mathf.Sqrt(Mathf.Pow(Mathf.Abs(end.x - start.x), 2) + Mathf.Pow(Mathf.Abs(end.y - start.y), 2));
    }

    //Set the value of the bullet
    public void setValue(int nr)
    {
        value = nr;
    }

    //Get the value of the bullet
    public int getValue()
    {
        return value;
    }

    //Get the bullets target
    public string getBulletTar()
    {
        return currTar;
    }

    //Detect collision with
    void OnTriggerEnter2D(Collider2D other)
    {
        //If the collision is with the monster, destroy the bullet
        if (other.tag == "Monster" && currTar == other.name)
        {
            AudioClip clip = GetComponent<AudioSource>().clip;
            Vector3 vec = transform.position;
            AudioSource.PlayClipAtPoint(clip, vec);

            Destroy(gameObject);
        }
    }

    //If the bullet leaves the game boundary, destroy it
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Boundary")
        {
            Destroy(gameObject);
        }
    }
}
