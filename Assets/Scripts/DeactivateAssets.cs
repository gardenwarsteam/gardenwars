﻿using UnityEngine;
using System.Collections;

public class DeactivateAssets : MonoBehaviour {
    public GameObject[] QuitButton = new GameObject[2];

	// Use this for initialization
	void Start () {
    #if UNITY_WEBGL
            foreach(GameObject button in QuitButton){
                button.SetActive(false);
            }
    #endif
    }
}
