﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using UnityEngine.SceneManagement;

public class HealthController : MonoBehaviour {
    private ArrayList plants = new ArrayList();
    private HUDController hudController;
    private DataHolder dataHolder;
    private GameObject UI;
    private ShowPanels ShPa;
    private Text DeathPoint;

    public float flashDuration = 0.75f;

    void Start () {
        foreach (Transform child in transform)
        {
            plants.Add(child);
        }

        hudController = GameObject.Find("HUD").GetComponent<HUDController>();
        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();

        UI = GameObject.Find("UI");
        ShPa = UI.GetComponent<ShowPanels>();
        DeathPoint = UI.transform.Find("DeathPanel/DeathPoint/DeathPointText").GetComponent<Text>();
    }

    //Function to remove health
    public void loseHealth(Vector3 pos)
    {
        hudController.animColorFlash.SetTrigger("flash");
       
        //StartCoroutine(hudController.Flash(flashDuration));

        Transform temp = (Transform)plants[0];
        Destroy(temp.gameObject);
        plants.RemoveAt(0);
		AudioClip clip = GetComponent<AudioSource>().clip;
        AudioSource.PlayClipAtPoint(clip, transform.position);

        if(plants.Count == 0)
        {
            dataHolder.setPoints(hudController.getPoints());
            DeathPoint.text = "" + hudController.getPoints();

            dataHolder.setHighscore(hudController.getPoints());
            dataHolder.setGamePlayed(true);

            Time.timeScale = 0;
            ShPa.ShowDeathScreen();
        }
    }
}
