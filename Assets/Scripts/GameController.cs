﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {


    public  ArrayList primes = new ArrayList(new float[] {       //Prime numbers under 100
        2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 } );

    private Transform RotatePart;           //The part of the Cannon that is to be rotated

    private Vector3 targetPos;              //Position data for the target
    private Quaternion targetRot;           //Rotation data for the target

    private Vector3 CanonPos;               //Position data for the Cannon
    private Quaternion CanonRot;            //Rotation data for the Cannon

    private GameObject targetObject;
    public  GameObject TargetingCircle;     //The targeting circle

    private string CurrTarget = "";         //Name of the current target

    //Variables for canon rotation
    private float lerpTime = 1f;
    private float currentLerpTime = 0f;

    private bool anyTarget = false;         //Whether there is a target or not

    public float monsterWidth = 1.5f;
    private float angleOffset;
    private bool withinTarget = false;

    // Use this for initialization
    void Start () {
        //Get the placement data of the Canon
        RotatePart = transform.Find("RotatePart").transform;

        //Get the current position and rotation
        CanonPos = RotatePart.position;
        CanonRot = RotatePart.rotation;
        targetRot = CanonRot;
    }
	
	// Update is called once per frame
	void Update () {
        //Left mouse click
        if (Input.GetMouseButtonDown(0))
        {
            //Set a ray between the mouse point and camera
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hitInfo = Physics2D.Raycast(ray.origin, ray.direction);

            //Check whether the object getting hit has a collider, and whether it
            //is the current object getting targeted
            if (hitInfo.collider != null && 
                CurrTarget != hitInfo.collider.name &&
                hitInfo.collider.tag == "Monster")
            {
                //Set target object
                targetObject = hitInfo.collider.gameObject;
                CurrTarget = targetObject.name;

                //Calculate the angle
                calcAngle();
                
                //int temp = int.Parse(targetObject.GetComponentInChildren<TextMesh>().text);
                //Debug.Log("Target: " + targetObject + ", on Position: " + targetPos);
                
                //Reset the time for lerp
                currentLerpTime = 0f;
            }
        }

        //Rotate towards the target using lerp
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
        }
        //lerp!
        float t = currentLerpTime / lerpTime;
        t = t * t * t * (t * (6f * t - 15f) + 10f);

        CanonRot = RotatePart.rotation;
        RotatePart.rotation = Quaternion.Slerp(CanonRot, targetRot, t);

        if (targetObject != null)
        {
            //Calculate angle again, as the targets keep moving
            calcAngle();

            anyTarget = false;

            float diff = Quaternion.Angle(CanonRot, targetRot);
            if ( diff < angleOffset)
            {
                withinTarget = true;
            }
            else
            {
                withinTarget = false;
            }
        }
        else if(!anyTarget) //If the target disappears, set the rotation to original
        {
            targetRot = Quaternion.identity;
            //Reset the time for lerp
            currentLerpTime = 0f;

            anyTarget = true;
            withinTarget = false;

            TargetingCircle.transform.position = new Vector3(0f, -5f);
        }
    }

    //Save pos and calculate angle
    private void calcAngle()
    {
        //Get the position of the target object
        targetPos = targetObject.transform.position;

        //Set the targeting circle
        TargetingCircle.transform.position = targetPos;

        //Calculate the angle
        float angle = getAngle(targetPos, CanonPos);
        
        angleOffset = Mathf.Atan2(monsterWidth, getLength(targetPos, CanonPos)) * Mathf.Rad2Deg;

        targetRot = Quaternion.AngleAxis(angle, Vector3.back);
    }

    //
    private float getAngle(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.x - b.x, a.y - b.y) * Mathf.Rad2Deg;
    }

    private float getLength(Vector3 start, Vector3 end)
    {
        return Mathf.Sqrt(Mathf.Pow(Mathf.Abs(end.x - start.x), 2) + Mathf.Pow(Mathf.Abs(end.y - start.y), 2));
    }

    public Vector3 getTargetPos()
    {
        return targetPos;
    }

    public bool getTargetBool()
    {
        return anyTarget;
    }

    public string getCurrentTar()
    {
        return CurrTarget;
    }

    public bool getwithinTarget()
    {
        return withinTarget;
    }
}
