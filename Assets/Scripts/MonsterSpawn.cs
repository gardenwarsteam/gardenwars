﻿using UnityEngine;
using System.Collections;

public class MonsterSpawn : MonoBehaviour {

    public GameObject Monster;              //Monster object
    private float targetCount = 0;          //Variable for the number of enemies spawned
    public int enemieNumbers;               //Total number of enemies in the scene

    private float nextSpawn;                //The last time an enemie spawned
    public float spawnRate = 5f;            //The rate at which the enemies should spawn

    //private float nextCheck = 5f;
    //private float checkAmount = 5f;         //time variable to check of spawntime should be changed

    public int[] divNumber = { 7 };
    private int[] possibleNumbers;

    public bool runTabelLevel = false;

    private float[] lastSpawnX = new float[2];

    DataHolder dataHolder;
    private int level = 0;
    levelData leveldata;

    void Start()
    {
        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();
        enemieNumbers = 0;

        level = dataHolder.getLevel();

        leveldata = dataHolder.getLevelData(level);
        runTabelLevel = leveldata.runTabelLevel;

        if (runTabelLevel)
        {
            divNumber = leveldata.tabels;
            //Only 9 numbers, don't take the number * one
            /*possibleNumbers = new int[9 * divNumber.Length];
            int count = 0;
            foreach (int numbers in divNumber)
            {
                testString += numbers + ", ";
                for (int i = 0; i < 9; i++)
                {
                    possibleNumbers[9 * count + i] = (i + 2) * numbers;
                }
                count++;
            }*/
        }
    }

	// Update is called once per frame
	void Update () {
        //Wait for the next time to spawn
        if (Time.time > nextSpawn)
        {
            //Check and set next spawn time
            checkSpawnRate();
            nextSpawn = Time.time + spawnRate;

            //Instantiates the monster in the scene
            //Note: The ranges has to be determined by the screen/camera size
            GameObject newmonster;

            float spawnX;// = Random.Range(-6.5f, 6.5f);
            //Ensures the monsters don't spawn to close to each to overlap
            bool toClose;
            do
            {
                spawnX = Random.Range(-6.5f, 6.5f);
                //Ensures monsters don't overlap
                toClose = Mathf.Abs(lastSpawnX[0] - spawnX) < 1f || Mathf.Abs(lastSpawnX[1] - spawnX) < 1f;
            } while (toClose);

            lastSpawnX[1] = lastSpawnX[0];
            lastSpawnX[0] = spawnX;

            newmonster = (GameObject)Instantiate(Monster, new Vector3(spawnX, 11f, 0f), Quaternion.identity);

            newmonster.name = "Monster" + targetCount;
            enemieNumbers++;

            //Set the number for the monster
            if (runTabelLevel)
            {
                int nr = divNumber[Random.Range(0, divNumber.Length)] * Random.Range(1, 11);
                newmonster.GetComponentInChildren<TextMesh>().text = "" + nr;
            }
            else
            {
                string text = "" + Random.Range(leveldata.Min, leveldata.Max+1);
                newmonster.GetComponentInChildren<TextMesh>().text = text;
            }

            targetCount++;
        }
    }

    public void setNewDiv(int[] newD)
    {
        divNumber = newD;
        dataHolder.setDivNumber(divNumber);
    }

    /*public void setCustomNum(bool cusNum)
    {
        CustomNumbers = cusNum;
        dataHolder.setCustom(true);
    }*/

    //Adjusts the spawnrate depending on the number of enemies in the scene
    public void checkSpawnRate()
    {
        if (targetCount >= 1)
        {
            //GameObject[] gos = GameObject.FindGameObjectsWithTag("Monster");
            if (enemieNumbers == 0)
            {
                spawnRate -= 1.0f;
            }
            else if (enemieNumbers <= 1)
            {
                spawnRate -= 0.7f;
            }
            else if (enemieNumbers <= 2)
            {
                spawnRate -= 0.3f;
            }
            else if (enemieNumbers >= 7)
            {
                spawnRate += 1;
            }
            else if (enemieNumbers >= 6)
            {
                spawnRate += 0.7f;
            }
            else if (enemieNumbers >= 5)
            {
                spawnRate += 0.5f;
            }
            else if (enemieNumbers >= 4)
            {
                spawnRate += 0.1f;
            }
        }
    }
}
