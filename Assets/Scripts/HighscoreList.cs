﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class HighscoreList : MonoBehaviour {

    private DataHolder dataHolder;
    public GameObject HighList;
    private Text[] highscorelist;
    public static HighscoreList Instance;

    private int currentLevelShown;

    public levelData[] allHighscores = new levelData[7];

    // Use this for initialization
    void Awake ()
    {
        //Debug.Log("Highscore awake called");
        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();

        highscorelist = HighList.GetComponentsInChildren<Text>();

        for(int i = 0; i<7; i++)
        {
            allHighscores[i] = dataHolder.getLevelData(i);
        }
    }

    public void updateHighList(int level)
    {
        if (level != currentLevelShown)
        {
            Debug.Log("Updating highscore panel for L" + level);
            int[] high = dataHolder.getHighscore(level);

            for (int i = 0; i < highscorelist.Length; i++)
            {
                highscorelist[i].text = "" + high[i];
            }
            currentLevelShown = level;
        }
    }

    //Updates the highscorelist on level load
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        currentLevelShown = -1;

        if (FindObjectsOfType(GetType()).Length == 1)
        {
            GameObject.Find("SceneDataHolder").GetComponent<localDataHandler>().LoadHighscores();
        }

        if ((Instance == this || Instance == null) && scene.buildIndex == 0)
        {
            Instance = this;
            Debug.Log("Highscore finished load called");
            updateHighList(0);
        }
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled.
        //Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
}
