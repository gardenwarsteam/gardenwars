﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimePlay : MonoBehaviour {

    private DataHolder dataHolder;
    public GameObject HUD;
    private HUDController hudCont;

    private GameObject UI;
    private ShowPanels ShPa;
    private Text FinalPoint;

    public float playTime = 60f;
    private float part;

    private bool fin = false;

    // Use this for initialization
    void Start ()
    {
        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();
        hudCont = HUD.GetComponent<HUDController>();

        UI = GameObject.Find("UI");
        ShPa = UI.GetComponent<ShowPanels>();
        FinalPoint = UI.transform.Find("FinishPanel/FinalPoint/FinalPointText").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Time defined gameplay
        if (Time.timeSinceLevelLoad <= playTime)
        {
            part = Time.timeSinceLevelLoad / playTime;

            hudCont.setProcessbar(part);
        }
        else
        if (!fin)
        {
            fin = true;
            dataHolder.setPoints(hudCont.getPoints());
            FinalPoint.text = "" + hudCont.getPoints();

            dataHolder.setHighscore(hudCont.getPoints());
            dataHolder.setGamePlayed(true);
            
            if(dataHolder.getLastActiveLevel() <= dataHolder.getLevel())
            {
                dataHolder.setLastActiveLevel(dataHolder.getLevel() + 1);
            }

            Time.timeScale = 0;
            ShPa.ShowFinishScreen();
        }

    }
}
