﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PointHandle : MonoBehaviour
{
    private DataHolder dataHolder;
    public GameObject pointField;
    private Transform PointObj;
    private Text pointText;
    public static PointHandle Instance;

    // Use this for initialization
    void Awake()
    {
        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();
        PointObj = pointField.transform.FindChild("PointText");
        pointText = PointObj.GetComponent<Text>();
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (Instance == this || Instance == null)
        {
            Instance = this;
            if (dataHolder.getGamePlayed() && scene.buildIndex == 0)
            {
                pointField.SetActive(true);
                pointText.text = "" + dataHolder.getPoints();
            }
            else
            {
                pointText.text = "";
                pointField.SetActive(false);
            }
        }
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled.
        //Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
}
