﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class localDataHandler : MonoBehaviour
{
    private DataHolder dataHolder;
    public static localDataHandler Instance;

    void Awake()
    {
        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();
    }

    public void LoadHighscores()
    {
        Debug.Log("Highscore loading from local");
        List<int> highscore = new List<int>();
        int levels = dataHolder.getNumberOfLevels();
        string temp = "highListL";
        for (int i = 0; i<levels; i++)
        {
            string tempLevel = temp + i + "nr";
            for(int j = 0; j<5; j++)
            {
                if (PlayerPrefs.HasKey(tempLevel + j))
                {
                    highscore.Add(PlayerPrefs.GetInt(tempLevel + j));
                }
                if (j >= 7)
                {
                    Debug.Break();
                }
            }
            dataHolder.setAllHighscoreList(i, highscore);
            highscore.Clear();

            if(i >= 7)
            {
                Debug.Break();
            }
        }
    }

    public void SaveHighscores(int level)
    {
        string temp = "highListL" + level + "nr";
        int[] scores = dataHolder.getHighscore(level);
        for(int i = 0; i<scores.Length; i++)
        {
            PlayerPrefs.SetInt(temp + i, scores[i]);
            if (i >= 7)
            {
                Debug.Break();
            }
        }
    }
}
