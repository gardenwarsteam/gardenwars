﻿using UnityEngine;
using System.Collections;

public class Enemie_Controller : MonoBehaviour {

    public float speed;                         //The speed of the monster
    private Rigidbody2D rb;                     //The body of the 

    private GameObject canon;                   
    private GameController gameController;      //Variable for holding the GameController
    private MonsterSpawn monsterSpawn;

    private ArrayList primes;                   //List of prime numbers

    private SpriteRenderer MonsterSprite;       //The sprite with the monster
    private TextMesh Number;                    //Where the written number in the monster is placed
    private Transform Bubble;                   //The bubble where the calculation is written
    public float bubbleWait = 1.0f;             //The time the bubble stays on screen

    private HUDController hudController;        //Reference to point in the HUD

    private HealthController healthController;  //Function to controll health

    private bool MonsterStopped = false;        //Depends on whether the monster is stopped or not
    
    public GameObject Explosion;


    // Use this for initialization
    void Start () {
        //Get the rigidbody2d component
        rb = GetComponent<Rigidbody2D>();
        //Set the speed and direction of the bullet
        rb.velocity = -transform.up * speed;

        Bubble = transform.Find("Bubble").transform;

        //Save the reference to the Gamecontroller
        canon = GameObject.Find("Canon");
        gameController = canon.GetComponent<GameController>();
        monsterSpawn = canon.GetComponent<MonsterSpawn>();


        //Get the list of the prime numbers
        primes = gameController.primes;

        hudController = GameObject.FindWithTag("HUD").GetComponent<HUDController>();

        Number = transform.Find("Number").GetComponent<TextMesh>();
        MonsterSprite = transform.Find("Monster").GetComponent<SpriteRenderer>();

        healthController = GameObject.Find("Life_Plants").GetComponent<HealthController>();
    }

    //Detect if something enters the collider
    //Check what is hitting the monster
    void OnTriggerEnter2D(Collider2D other)
    {
        //Destroy the monster if it hits the HitZone
        if (other.tag == "HitZone")
        {
            healthController.loseHealth(transform.position);
            Destroy(gameObject);
        }
        //If the monster is hit with the bullet and it is the current target
        //Won't do anything if the bubble is currently showing (Paused)
        else if (other.tag == "Bullet" && !MonsterStopped &&
            other.gameObject.GetComponent<Bullet_move>().getBulletTar() == transform.name)
        {
            //Stop the monster
            rb.velocity = Vector3.zero;

            MonsterStopped = true;

            //Get the different values from the bullet
            float bulNum = other.gameObject.GetComponent<Bullet_move>().getValue();
            //Get the different values from the monster
            float monNum = float.Parse(Number.text);

            //If the shot is a prime
            if (bulNum == 0)
            {
                if (primes.IndexOf(monNum) != -1)
                {
                    hudController.setPoints(500);

                    revDivRes(true, true, "Prim");
                }
                else
                {
                    hudController.setPoints(-200);

                    revDivRes(false, false, "Prim");
                }
            }
            else
            {
                //Calculate the division
                float result = monNum / bulNum;
                string calc = monNum + " : " + bulNum;
                //Debug.Log(monNum + " : " + bulNum + " = " + result);

                //If the result is 1, the monster and bullet have the same value
                if (result == 1)
                {
                    hudController.setPoints(300);

                    revDivRes(true, true, calc);
                }
                //If the monster and bullet have the same value
                else if (result % 1 == 0)
                {
                    //Print the result in the monster
                    Number.text = result.ToString();

                    //Get all possible divisions
                    ArrayList divs = findDiv(monNum);

                    //Give highest amount of points if the highest number is chosen for division
                    if (divs.Count - 1 == divs.IndexOf(bulNum) && divs.Count > 1)
                    {
                        hudController.setPoints(300);
                    }
                    else
                    {
                        hudController.setPoints(100);
                    }

                    revDivRes(false, true, calc);
                }
                //If the result is not an integer
                else
                {
                    hudController.setPoints(-100);

                    revDivRes(false, false, calc);
                }
            }
        }
    }

    //Return a list of all possible divisons
    ArrayList findDiv(float nr)
    {
        ArrayList divs = new ArrayList();
        for (float i = 2; i < 10; i++)
        {
            if ((nr / i) % 1 == 0)
            {
                divs.Add(i);
            }
        }

        return divs;
    }

    /// <summary>
    /// Function to resolve the division result and color of bubble
    /// </summary>
    /// <param name="dead">Whether the monster is dead or not</param>
    /// <param name="correct">Whether a correct number was input</param>
    /// <param name="tekst">The text for the bubbel</param>
    void revDivRes(bool dead, bool correct, string text)
    {
        //Let the number fade out. Otherwise it will show ontop
        Color color = Number.color;
        color.a = 0f;
        Number.color = color;

        //Make the bubble show
        Bubble.localScale = Vector3.one;

        //Set the bubble text
        Bubble.Find("Calculation").GetComponent<TextMesh>().text = text;
        
        //Get the color object for the bubble
        color = Bubble.Find("poof").GetComponent<SpriteRenderer>().color;

        //Change the color depending what it is
        if (correct)
        {
            //Green
            color = new Color(0f, 1f, 0f);

            if (dead)
            {
                Instantiate(Explosion, transform.position, transform.rotation);

                //Adjusts the spawnrate
                monsterSpawn.enemieNumbers--;

                MonsterSprite.enabled = false;
                AudioClip clip = GetComponent<AudioSource>().clip;
                AudioSource.PlayClipAtPoint(clip, transform.position);
            }
        }
        else
        {
            //Red
            color = new Color(1f, 0f, 0f);
        }

        //Set the new color
        Bubble.Find("poof").GetComponent<SpriteRenderer>().color = color;

        //Start the coroutine
        StartCoroutine(startUp(dead));
    }

    //Make the monster move again after a short while
    public IEnumerator startUp(bool dead)
    {
        // waits a little seconds
        yield return new WaitForSeconds(1.0f);

        if (dead)
        {
            Destroy(gameObject);
        }
        else
        {
            //Minimize the bubble
            Bubble.localScale = Vector3.zero;

            //Make the number visible again
            Color color = Number.color;
            color.a = 1f;
            Number.color = color;

            //Start the monster again
            rb.velocity = -transform.up * speed;

            MonsterStopped = false;
        }
    }

    //If the Monster leaves the game boundary, destroy it (should not happen in normal gameplay)
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Boundary")
        {
            Destroy(gameObject);
        }
    }
}
