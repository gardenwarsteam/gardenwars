﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDController : MonoBehaviour {

    private Transform PointObj;
    private Text pointText;
    public int Points = 0;

    [HideInInspector]
    public Animator animColorFlash;
    public AnimationClip flashColorAnimationClip;

    private Image ProgressImage;

    // Use this for initialization
    void Start () {
        PointObj = transform.FindChild("Points");
        pointText = PointObj.GetComponent<Text>();
        pointText.text = "0";

        ProgressImage = GameObject.Find("ProgressImage").GetComponent<Image>();
    }
	
    //Sets the amount of fill in the processbar
    public void setProcessbar(float i)
    {
        ProgressImage.fillAmount = i;
    }

    //Function to set the private variable Points
    public void setPoints(int i)
    {
        Points += i;
        pointText.text = Points.ToString();
    }

    //Function to get the private variable Points
    public int getPoints()
    {
        return Points;
    }
}
