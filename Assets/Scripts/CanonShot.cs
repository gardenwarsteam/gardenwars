﻿using UnityEngine;
using System.Collections;

public class CanonShot : MonoBehaviour {

    //public Rigidbody2D Bullet;
    public GameObject Bullet;           //The bullet prefab
    GameController gameController;      //Variable for holding the GameController

    public float fireRate = 0.25f;      //The rate the player can fire the cannon
    private float nextFire;             //The time between each possible shot

    private bool noTarget;              //Bool for whether there is a target of not
    private bool withinTarget;

    // Use this for initialization
    void Start () {
        gameController = GameObject.Find("Canon").GetComponent<GameController>();
    }
	
	// Update is called once per frame
	void Update () {
        //Save the status of the target
        noTarget = gameController.getTargetBool();
        withinTarget = gameController.getwithinTarget();

        //Don't fire if not the time for it or no target
        if (Time.time > nextFire && !noTarget && withinTarget)
        {
            foreach (char c in Input.inputString)
            {
                if (c >= '2' && c <= '9')
                {
                    nextFire = Time.time + fireRate;
                    Shot(c - '0');
                }
                else if (c == ' ')
                {
                    nextFire = Time.time + fireRate;
                    Shot(0);
                }
            }
        }
    }

    //https://unity3d.com/learn/tutorials/projects/space-shooter-tutorial/creating-shots?playlist=17147
    //The functions which handles the shot
    public void Shot(int nr)
    {
        //Instantiates the bullet in the scene
        GameObject bulletClone = (GameObject)Instantiate(Bullet, transform.position, transform.rotation);
        //Debug.Log(transform.name);
        bulletClone.GetComponent<Bullet_move>().setValue(nr);
        GetComponent<AudioSource>().Play();
    }
}
