﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class levelData
{
    public int levelNR;
    public int[] tabels;
    public bool runTabelLevel;
    public int Min;
    public int Max;
    public List<int> Highscore = new List<int>();

    /// <summary>
    /// Makes a level with numbers from the different multiplikation tabels
    /// </summary>
    /// <param name="level">The level number</param>
    /// <param name="tabel">The different numbers for the tabels</param>
    public levelData(int level, int[] tabel)
    {
        levelNR = level;
        tabels = tabel;
        runTabelLevel = true;
    }

    /// <summary>
    /// Makes a level with all numbers ranging from Min to Max
    /// </summary>
    /// <param name="level">The level number</param>
    /// <param name="min">The minimum number to spawn</param>
    /// <param name="max">The maximum number to spawn</param>
    public levelData(int level, int min, int max)
    {
        levelNR = level;
        Min = min;
        Max = max;
        runTabelLevel = false;
    }
}

public class DataHolder : MonoBehaviour {

    static int[] divNumber = { 2, 3, 5 };
    static bool CustomNumbers = true;
    static int Points = 0;
    static bool gamePlayed = false;
    static int currentLevel = 0;
    static int lastCompletedLevel = 0;        //Used to activate the level buttons
    
    //[HideInInspector]
    public static levelData[] AllLevels = new levelData[]
    {
        new levelData(0, new int[]{ 2, 3, 5 } ),
        new levelData(1, new int[]{ 2, 3, 4, 5, 6 } ),
        new levelData(2, new int[]{ 2, 3, 4, 5, 6, 9} ),
        new levelData(3, new int[]{ 2, 3, 4, 5, 6, 7, 8, 9} ),
        new levelData(4, 2, 50 ),
        new levelData(5, 50, 99 ),
        new levelData(5, 2, 99 )
    };

    public void setDivNumber(int[] divs)
    {
        divNumber = divs;
    }

    public int[] getDivNumber()
    {
        return divNumber;
    }

    public void setCustom(bool cusNum)
    {
        CustomNumbers = cusNum;
    }

    public bool getCustom()
    {
        return CustomNumbers;
    }

    public void setPoints(int poi)
    {
        Points = poi;
    }

    public int getPoints()
    {
        return Points;
    }

    public bool getGamePlayed()
    {
        return gamePlayed;
    }

    public void setGamePlayed(bool gamP)
    {
        gamePlayed = gamP;
    }

    public int getLevel()
    {
        return currentLevel;
    }

    public void setLevel(int level)
    {
        currentLevel = level;
    }

    public int getLastActiveLevel()
    {
        return lastCompletedLevel;
    }

    public void setLastActiveLevel(int level)
    {
        lastCompletedLevel = level;
    }

    public void setHighscore(int poi)
    {
        Debug.Log("Writing to level " + currentLevel + " highscore");
        AllLevels[currentLevel].Highscore.Add(poi);
        AllLevels[currentLevel].Highscore.Sort((a, b) => -1 * a.CompareTo(b));

        if(AllLevels[currentLevel].Highscore.Count > 5)
        {
            AllLevels[currentLevel].Highscore.RemoveAt(5);
        }

        //DEBUG
        string temp = "";
        foreach (int nr in AllLevels[currentLevel].Highscore)
        {
            temp += nr + ", ";
        }
        Debug.Log(temp);

        GetComponent<localDataHandler>().SaveHighscores(currentLevel);
    }
    
    public void setAllHighscoreList(int level, List<int> score)
    {
        AllLevels[level].Highscore = new List<int>(score);
    }

    public int[] getHighscore(int level)
    {
        Debug.Log("Getting highscore from level " + level);
        List<int> high = AllLevels[level].Highscore;
        int[] hi = new int[5];
        for (int i = 0; i<5; i++)
        {
            if(high.Count > i)
            {
                hi[i] = high[i];
            }
            else
            {
                hi[i] = 0;
            }
        }

        //DEBUG
        string temp = "";
        foreach (int nr in high)
        {
            temp += nr + ", ";
        }
        Debug.Log(temp);

        return hi;
    }

    public levelData getLevelData(int level)
    {
        return AllLevels[level];
    }

    public int getNumberOfLevels()
    {
        return AllLevels.Length;
    }
}
