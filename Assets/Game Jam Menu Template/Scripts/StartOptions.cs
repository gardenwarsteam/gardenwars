﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class StartOptions : MonoBehaviour
{
    // These variables are public and can be accessed in the inspector.
    public int sceneToStart = 1;                                        //Variable to determine which scene to play next.

    // These variables are still public, but not visible in the inspector
    [HideInInspector]
    public bool inMainMenu = true;                                      //Variable that disables the pause button in main menu
    [HideInInspector]   
    public Animator animColorFade;                                      //Animator which will fade to and from black when starting game.
    [HideInInspector]
    public Animator animMenuAlpha;                                      //Animator that will fade out alpha of MenuPanel canvas group
    public AnimationClip fadeColorAnimationClip;                        //Animation clip fading to color (black default) when changing scenes
    [HideInInspector]
    public AnimationClip fadeAlphaAnimationClip;                        //Animation clip fading out UI elements alpha

    // This is a private variable for this script only
    private ShowPanels showPanels;                                      //Reference to ShowPanels script on UI GameObject, to show and hide panels

    private DataHolder dataHolder;
    public static StartOptions Instance;

    /*******************************************************************/

    // Method that saves a reference to the showpanels element a soon as this script is run.
    // input: none
    // output: none, but a reference is now stored locally within this script
    void Awake()
    {
        showPanels = GetComponent<ShowPanels>();

        dataHolder = GameObject.Find("SceneDataHolder").GetComponent<DataHolder>();
    }

    // Method that shows the level selector as soon as the start button is clicked
    // input: none
    // output: none, but level selector is activated in the game
    public void StartButtonClicked()
    {
        showPanels.ShowLevelSelect();
    }
    /* Below are four methods, one for each level, they work exactly the same, so a description is only given for the first one */

    // Method that updates the scene to be loaded next and then continues the game start process
    // input: none
    // output: none, but the correct scene is found and the next function is called to start the game
    public void Level(int i)
    {
        Debug.Log("0" + i);
        sceneToStart = 1;
        dataHolder.setLevel(i);
        ContinueStart();
        showPanels.HideLevelSelect();
    }

    // Method that continues the start of the game, after the correct level has been determined
    // input: none
    // output: none, but next function (loadDelayed) is called.
    public void ContinueStart()
    {
        //Use invoke to delay calling of LoadDelayed by half the length of fadeColorAnimationClip
        Invoke("LoadDelayed", fadeColorAnimationClip.length * .5f);

        //Set the trigger of Animator animColorFade to start transition to the FadeToOpaque state.
        animColorFade.SetTrigger("fade");
    }

    // Method that finally loads the next scene
    // input: none
    // output: none, but the correct UI elements are shown and the correct scene is loaded
    public void LoadDelayed()
    {
        //Pause button now works if p is pressed since we are no longer in Main menu.
        inMainMenu = false;

        //Hide the main menu UI element
        showPanels.HideMenu();
        showPanels.ShowBar();
        showPanels.HideLastPoint();

        //Load the selected scene, by scene index number in build settings
        SceneManager.LoadScene(sceneToStart);
    }

    // Method that is called automatically and activates correct menues when GOING BACK to start scene after the game
    // input: none
    // output: none, but correct menues are shown
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (Instance == this || Instance == null)
        {
            Debug.Log("Inside load Startoptions");
            Instance = this;
            if (SceneManager.GetActiveScene().name == "Start")
            {
                showPanels.ShowMenu();
                showPanels.HideBar();

                if (dataHolder.getGamePlayed())
                {
                    showPanels.ShowLevelSelect();
                }

                showPanels.HideFinishScreen();
                showPanels.HideDeathScreen();
                showPanels.HideLevelButtons();
                showPanels.ShowLevelButtons(dataHolder.getLastActiveLevel());

                inMainMenu = true;
            }
            else
            {
                inMainMenu = false;
            }

            showPanels.HidePausePanel();

            if (SceneManager.GetActiveScene().name == "Main" && !dataHolder.getGamePlayed())
            {
                //Invoke("delayedPause", fadeColorAnimationClip.length * .5f);

                showPanels.ShowControls();
                Time.timeScale = 0;
            }
        }
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled.
        //Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    public void LoadStartMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void delayedPause()
    {
        showPanels.ShowControls();
        Time.timeScale = 0;
    }
}
