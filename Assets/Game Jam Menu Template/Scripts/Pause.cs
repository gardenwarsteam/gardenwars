﻿using UnityEngine;

public class Pause : MonoBehaviour
{

    // Private variables only to be seen and used inside the script
    private ShowPanels showPanels;                      //Reference to the ShowPanels script used to hide and show UI panels
    private bool isPaused;                              //Boolean to check if the game is paused or not
    private StartOptions startScript;                   //Reference to the StartButton script

    //Awake is called automatically as soon as the script is active (and only once) to get correct references
    // input: none
    // output: none, but refences are defined.
    void Awake()
    {
        showPanels = GetComponent<ShowPanels>();
        startScript = GetComponent<StartOptions>();
    }

    // Update is called once per frame and checks for user input to call pause function
    // input: none (as parameter) but method checks for pressed keys
    // output: none, but game is paused or unpaused based on keys
    void Update()
    {
        //Check if the p are down this frame and that game is not paused, and that we're not in main menu
        if (Input.GetKeyDown("p") && !isPaused && !startScript.inMainMenu)
        {
            DoPause();
        }
        //If the button is pressed and the game is paused and not in main menu
        else if (Input.GetKeyDown("p") && isPaused && !startScript.inMainMenu)
        {
            UnPause();
        }

    }

    // Method that pauses the game 
    // input: none
    // output: none, but pause variable is set, pause panel is shown, and game is paused
    public void DoPause()
    {
        isPaused = true;
        //Set time.timescale to 0, this will cause animations and physics to stop updating
        Time.timeScale = 0;
        showPanels.ShowPausePanel();
    }

    // Method that pauses the game
    // input: none
    // output: none, but game is unpause, pause variable is set, and panels are hidden
    public void UnPause()
    {
        isPaused = false;
        //Set time.timescale to 1, this will cause animations and physics to continue updating at regular speed
        Time.timeScale = 1;
        showPanels.HidePausePanel();
    }
}
