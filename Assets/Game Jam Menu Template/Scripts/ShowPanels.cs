﻿/*
 * In this script showing and hiding of panels is prepared through simple methods.
 */
using UnityEngine.UI;
using UnityEngine;

public class ShowPanels : MonoBehaviour
{
    // Store references to the correct objects, so that they can be accessed later in the script.
    public GameObject optionsPanel;
    public GameObject optionsTint;
    public GameObject menuPanel;
    public GameObject pausePanel;
    public GameObject MenuBar;
    public GameObject ControlIMG;
    public GameObject LevelSelect;
    public GameObject PointField;
    public GameObject HighscorePanel;
    public GameObject[] LevelButtons;
    public GameObject LevelFinish;
    public GameObject DeathFinish;

    // These methods are called to either hide or show the above defined objects.
    public void ShowOptionsPanel()
    {
        optionsPanel.SetActive(true);
        optionsTint.SetActive(true);
    }

    public void HideOptionsPanel()
    {
        optionsPanel.SetActive(false);
        optionsTint.SetActive(false);
    }

    public void ShowMenu()
    {
        menuPanel.SetActive(true);
    }
    
    public void HideMenu()
    {
        menuPanel.SetActive(false);
    }
    
    public void ShowPausePanel()
    {
        pausePanel.SetActive(true);
        optionsTint.SetActive(true);
    }

    public void HidePausePanel()
    {
        pausePanel.SetActive(false);
        optionsTint.SetActive(false);
    }

    public void ShowBar()
    {
        MenuBar.SetActive(true);
    }

    public void HideBar()
    {
        MenuBar.SetActive(false);
    }

    public void ShowControls()
    {
        ControlIMG.SetActive(true);
    }

    public void HideControls()
    {
        ControlIMG.SetActive(false);
    }

    public void ShowLevelSelect()
    {
        LevelSelect.SetActive(true);
    }

    public void HideLevelSelect()
    {
        LevelSelect.SetActive(false);
    }

    public void HideLastPoint()
    {
        PointField.SetActive(false);
    }

    public void HideHighscore()
    {
        HighscorePanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    public void ShowHighscore()
    {
        HighscorePanel.SetActive(true);
        menuPanel.SetActive(false);
    }

    public void ShowLevelButtons(int MinLevelToStart)
    {
        for(int i = 0; i < LevelButtons.Length && i <= MinLevelToStart; i++)
        {
            LevelButtons[i].GetComponent<Button>().interactable = true;
        }
    }

    public void HideLevelButtons()
    {
        for (int i = 0; i < LevelButtons.Length; i++)
        {
            LevelButtons[i].GetComponent<Button>().interactable = false;
        }
    }

    public void ShowFinishScreen()
    {
        LevelFinish.SetActive(true);
    }

    public void HideFinishScreen()
    {
        LevelFinish.SetActive(false);
    }

    public void ShowDeathScreen()
    {
        DeathFinish.SetActive(true);
    }

    public void HideDeathScreen()
    {
        DeathFinish.SetActive(false);
    }
}
