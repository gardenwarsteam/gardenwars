﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class Restarting : MonoBehaviour
{
    // Only one private variable is needed - the one for accessin the pause script.
    private Pause pausescript;

    // method that runs automatically as soon as the script is active. This get's the correct reference to pause
    // input: none
    // output: none, but pause has now a proper reference
    void Awake()
    {
        pausescript = GetComponent<Pause>();
    }

    // method that restarts the game by loading scene 1 again and unpausing the game
    // input: none
    // output: none, but game is restarted and unpaused
    public void Restart()
    {
        Debug.Log("Now Restarting");
        pausescript.UnPause();
        SceneManager.LoadScene(1);
        
    }
}
