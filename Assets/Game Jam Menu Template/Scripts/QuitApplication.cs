﻿/* This code is based on a menu template provided by Unity3D
 * 
 * This specific script ensures that the application is 
 * shut down approprately, depending on whether it is running
 * as a standalone program or still as part of Unity.
 * 
 * Script uses precompiler to determine which code snipped is
 * to be compiled, so that no unnecessary bits are compiled.    */
using UnityEngine;

public class QuitApplication : MonoBehaviour {
    /* method that is called when user clicks on Quit button
     * input: none, Unity implicitly defines the game mode
     * output: none, but game is shut down                      */
	public void Quit()
	{
		//If we are running in a standalone build of the game
	#if UNITY_STANDALONE
		//Quit the application
		Application.Quit();
	#endif

		//If we are running in the editor
	#if UNITY_EDITOR
		//Stop playing the scene
		UnityEditor.EditorApplication.isPlaying = false;
	#endif
	}
}
